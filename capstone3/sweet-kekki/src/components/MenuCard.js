import UserContext from '../UserContext.js';

import {Container, Row, Col, Card, Button} from 'react-bootstrap';

import {Link, useParams} from "react-router-dom";

import {useState, useEffect, useContext} from 'react';

import Swal2 from 'sweetalert2';

export default function MenuCard({showDetails, ...props}) {

	const {user} = useContext(UserContext);

    const {id} = useParams();

	 console.log(props.productProp);

	const [count, setCount] = useState(0);

	const [order, setOrder] = useState(50);

    const [isDisabled, setIsDisabled] = useState(false);

  const {productId, name, description, price} = props.productProp;

	function menu(){
		if(order !== 0) {
			setCount(count + 1);
			setOrder(order - 1);
		}else{
			setIsDisabled(true);
			return ("Out of Stock!");
		}
	}

	useEffect(()=> {
		if (order === 0){
			setIsDisabled(true);
		}
	}, [order]);

   return (
   	 <Container className= 'mt-3 menu-bg'>
            <Row>
                <Col className= "mt-3"> 
                   <Card className = "cardHighlight productImage">
                        <Card.Body className = 'mt-5'>

                          <Card.Title>{name}</Card.Title>

                          <Card.Text>
                          <strong>Description</strong> :
                            {description}
                          </Card.Text>

                          <Card.Text>
                          <strong>Price</strong> :         
                             {price}
                          </Card.Text>
                          
                          <Card.Subtitle>Orders :</Card.Subtitle>
                          <Card.Text>{count}</Card.Text>

                          {
                            user !== null ?

                            <Button onClick = {showDetails}>Details</Button>

                            :

                            <Button as = {Link} to = '/login'>Login to email</Button>

                          }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
   	)
}