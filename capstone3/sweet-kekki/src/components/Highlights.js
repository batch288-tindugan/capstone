import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Highlight(){
	return (
       <Container className = 'mt-5 mb-3 '>
       	  <Row>

       	  	<Col className = "col-md-4 mt-3 ">
                <Card className = "cardHighlight b-day">
                        <Card.Body>
                          <Card.Title className = "land-txt txt">Birthday Blow-up</Card.Title>
                          <Card.Text className = "txt"> 
                            Special Discount up to 30% Off when you dine with family on day of you Birthday!
                          </Card.Text>
                        </Card.Body>
                    </Card>
       	  		
       	  	</Col>

       	  	<Col className= "col-md-4 mt-3">

                    <Card className = "cardHighlight delivery">

                        <Card.Body>
                           <Card.Title className = "land-txt txt">Free Home Delivery</Card.Title>
                           <Card.Text className = "txt"> 
                           Quick Delivery around Manila!
                           </Card.Text>
                           <Button className = "buttons order-button" as = {Link} to = '/menu'>Order Now!</Button>
                        </Card.Body>
                    </Card>

                </Col>

                <Col className= "col-md-4 mt-3 "> 

                    <Card className = "cardHighlight shop">

                       <Card.Body>
                           <Card.Title className = "land-txt txt">Sweet Kekki</Card.Title>
                           <Card.Text className = "txt">
                                1234-456-7890 
                           </Card.Text>
                           <Card.Text className = "txt">
                                hello@Batch228.com
                           </Card.Text>
                           <Card.Text className = "txt">
                               123 saBahay ninyo St., Ewan city
                           </Card.Text>

                       </Card.Body>
                    </Card>
                    
                </Col>
       	  </Row>
       </Container>
		)
}