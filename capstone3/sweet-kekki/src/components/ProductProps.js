import {Button, Card, Col, Container, Row, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link, useParams, useNavigate} from 'react-router-dom';

export default function ProductProps({onUpdate, onDisable, onEnable, ...props}){

    const {_id, name, description, quantity, isActive, price} = props.productProp;

	 const navigate = useNavigate();

   const [isDisabled, setIsDisabled] = useState(false);

	useEffect(() => {
		if(name !== '' && description !== '' && quantity !== '' && price !== ''){
           setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [name, description, quantity, price])

   
	return(

        <tr>
            <td>{name}</td>
            <td colSpan={5}>
               {description}
            </td>
            <td>{quantity}</td>
            <td>{price}</td>
            <td>{isActive ? "available" : "not available"}</td>
            <td>
            <Button variant="primary" className = 'custom-button' onClick = {onUpdate}>Update</Button>{' '}
            {
              isActive ? 
              <Button variant="danger" className = 'custom-button' onClick = {onDisable}>Disable</Button>
              :
              <Button variant="danger" className = 'custom-button' onClick = {onEnable}>Enable</Button>
            }
            </td>

       </tr>
)
}