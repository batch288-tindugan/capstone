import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function PageNotFound() {
    return (
        <Container>
            <Row>
                <Col>
                    <div className = "text-center mt-5">
                     <h1>Page Not Found</h1>
                     <p>go back to the <Link to ='/'>Home page</Link></p>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

