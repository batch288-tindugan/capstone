import Banner from '../components/Banner.js';

import Highlights from '../components/Highlights.js';

import MenuCard from '../components/MenuCard.js';

export default function Home(){

	return(
		<>
			<Banner/>
			<Highlights/>
		</>
		)
}