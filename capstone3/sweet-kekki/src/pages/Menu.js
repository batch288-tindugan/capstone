import productData from '../data/product.js';
import MenuCard from '../components/MenuCard.js';
import {useNavigate, useParams} from 'react-router-dom';

import {useState, useEffect} from 'react';
export default function Product (props){
   
   const navigate = useNavigate();

   const {id} = useParams();

	const [products, setProducts] = useState ([]);
	useEffect(() => {
       fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
       .then(result => result.json())
       .then(data => {
       	    setProducts(data.map(product => {
       	    	return(

       	    		<MenuCard 
                  key = {product._id} 
                  productProp = {product} 
                  showDetails = {() => (navigate(`/orders/${product._id}`))} 
                  />

       	    		)
       	    }))
       })
	}, [])
 	return (
	  <>
      <h1 className = 'text-center mt-3 menu'>Menu</h1>
         {products}
     </>
)
 		
}
