import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Checkout(){

 const [name, setName] = useState('');
 const [description, setDescription] = useState('');
 const [price, setPrice] = useState('');
 const [quantity, setQuantity] = useState(0);
 const [total, setTotal] = useState(price);

 const navigate = useNavigate();

 const {id} = useParams();
// console.log(id);

const minus = (e) => {
  setQuantity(quantity -1)
  setTotal((quantity -1) * price)
}

const plus = (e) => {
  setQuantity(quantity +1)
  setTotal((quantity +1) * price)
}

 useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
    .then(result => result.json())
    .then(data => {
      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
      setTotal(data.price)
      setQuantity(1);
      // console.log(data);
    })
 }, [])

  const checkout = (id) => {
     console.log(quantity);
     fetch(`${process.env.REACT_APP_API_URL}/orders/${id}`, {
            method: "POST",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem("token")}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                products: [{
                    quantity: quantity
                }]
            })
        })
        .then(result => result.json())
        .then(data => {
          console.log(data);
            if(data){

                Swal2.fire({
                    title: "Order successful",
                    icon: "success",
                    text: 'Thank you for buying our product'
                })

                navigate("/menu")

            } else {

                Swal2.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: 'Please try again'
                })
            }

        })
  }

 return(
    <Container>
    <div className = 'checkout'>
      <Row>
        <Col className = 'col-10 checkout-text'>

          <h1 className = 'text-center'>Checkout Order</h1>
          <Card className="text-center cardHighlight order">
            <Card.Body>
            <Card.Title>{name}</Card.Title>

            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>
            {description}
            </Card.Text>
            
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>PhP
            {price}
            </Card.Text>
             
            <Card.Subtitle>Quantity:</Card.Subtitle>
            <Card.Text className ='quantity'>
            <FontAwesomeIcon icon={faMinus} style={{color: "#ec4b4b",}} className = "me-2" onClick = {quantity === 0 ? null
              :
             e => minus(e)} />
            {quantity}
            <FontAwesomeIcon icon={faPlus} style={{color: "#ec4b4b",}} className = "ms-2" onClick = {e => plus(e)} />
            </Card.Text> 
              
            <Card.Subtitle>Total:</Card.Subtitle>
            <Card.Text>
            {total}
            </Card.Text>

            </Card.Body>
            <Card.Footer className="text-muted">
            <Button 
            variant="primary"
            onClick = {() => checkout(id)}
            className="checkout-btn"
            >Check Out</Button>
            </Card.Footer>
          </Card>

        </Col>
      </Row>
    </div>
    </Container>
 )
}