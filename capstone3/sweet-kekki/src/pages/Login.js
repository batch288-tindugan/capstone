import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {Navigate, Link, useNavigate} from 'react-router-dom';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import Swal2 from 'sweetalert2';

export default function Login(){
	const [email, setEmail] = useState('');
	const [Password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

  const navigate = useNavigate();
	
	const {user, setUser} = useContext(UserContext);

	const retrieveUserdetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/user/userDetails`, {method: 'GET',
			headers:{
				Authorization: `Bearer ${token}`
			}
	  })
	  .then(result => result.json())
	  .then(data => {
	  	console.log(data);
	  	setUser({
	  		id : data._id,
	  		isAdmin: data.isAdmin
	  	});
	  	 if (data.isAdmin){
				 	navigate('/dashboard')
				 } else{
				 	navigate('/')
				 }
	  })
	}

	useEffect(() => {
		if (email !== '' && Password !== "" && Password.length > 6) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, Password]);

	function login(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: Password
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
					title: 'Authorization failed!',
					icon: 'error',
					text: 'Check your login details and try again!'
				})
			} else {
	
				localStorage.setItem('token', data.auth);

				retrieveUserdetails(data.auth);

				Swal2.fire({
					title : 'Login successfully',
					icon : 'success',
					text : 'Welcome to Sweet Kekki!'
				})
			}
		})
	}

	return (

          <Container className = "mt-5 bg-login">
          	<Row>
          		<Col className = "col-6 mx-auto mt-5">
          		   <h1 className = "text-center mb-5">Login</h1>

          		   <Form onSubmit = {event => login(event)} className = 'login-text'>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address : </Form.Label>
        <Form.Control type="email" value = {email} onChange = {event => setEmail(event.target.value)}placeholder="Enter email" />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password : </Form.Label>
        <Form.Control type="password" value = {Password}
        onChange = {event => setPassword(event.target.value)} placeholder="Password" />
      </Form.Group>
       
       <p>No account yet? <Link to ='/register'>Sign up here</Link></p>

      <Button variant="primary" type="submit" disabled = {isDisabled} className = "login-btn">
        Submit
      </Button>
    </Form>
          		</Col>
          	</Row>
          </Container> 
          
		)
}