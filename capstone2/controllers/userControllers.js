const User = require("../models/Users.js");

const bcrypt = require("bcrypt");

const auth = require('../auth.js');

//Controllers

module.exports.userRegistration = (request, response) => {

	User.findOne({email : request.body.email})
	.then(result => {

		if (result) {

			return response.send(false)
		} else {

			let newUser = new User({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNo: request.body.mobileNo
				})

			newUser.save()
			return response.send(true);
		}
	})
	.catch(error => response.send(false));
}

module.exports.loginUser = (request, response) => {
    User.findOne({email: request.body.email})
    .then(result => {

        if(!result){
            return response.send(false)
        } else {

            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

            if(isPasswordCorrect){
                return response.send({
                    auth: auth.createAccessToken(result)
                })
            } else {

                return response.send(false)
            }
        }
    })
    .catch(error => response.send(false));
}

module.exports.getProfile = (request, response) => {

	User.findById(request.body.id)
	.then(result => {

		const idDetails = auth.decode(request.headers.authorization);

		if(idDetails.isAdmin){
			password: result.password = `*****`
			return response.send(result)
		} else{
			return response.send(false)
		}
	}).catch(error => response.send(false))
}

module.exports.getUserDetails = (request, response) => {
    
    const userData = auth.decode(request.headers.authorization);

	User.findById(userData.id)
	.then(result => {

		result.password = `*****`
			return response.send(result)
	}).catch(error => response.send(false))
}