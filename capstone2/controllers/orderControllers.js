const Orders = require('../models/orders.js');

const auth = require('../auth.js');

const Products = require('../models/products.js');


//for creating/adding order
module.exports.createOrder = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const quantity = request.body.products[0].quantity;
    console.log(request.body.products[0].quantity);
    if(userData.isAdmin){

        return response.send(false)

    } else {

        Products.findById({_id: request.params.id})
        .then(result => {

            let newOrder = new Orders({
                userId: userData.id,
                products: [{
                    productId: result.id,
                    price: result.price,
                    quantity: quantity
                }],
                total: (quantity*result.price)
            })

            newOrder.save()
            .then(saved => response.send(true))
            .catch(error => response.send(false))

        })
    }
}

