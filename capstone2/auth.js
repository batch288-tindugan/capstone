const jwt = require("jsonwebtoken");

const secret = "SweetKekkiAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}

	return jwt.sign(data, secret,  {});
}


module.exports.verify = (request, response, next) => {


	let token = request.headers.authorization;

//"Bearer "
	if(token !== undefined){

		token = token.slice(7, token.length);

		//Validate the token busing the "verify" method in decrypting the token using the secret code.
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return response.send(false)
			}else{
				
				next();
			}
		})
	}else{
		return response.send(false)
	}

}

//decode the encrypted token.
module.exports.decode = (token) => {
	token = token.slice(7, token.length);

	//The decode method is used to obtain the infromation from the JWT
	//The {complete: true} option allows us to return additional information from the JWT token


	return jwt.decode(token, {complete: true}).payload;
}