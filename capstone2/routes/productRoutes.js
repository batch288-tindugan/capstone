const express = require("express");

const productControllers = require("../controllers/productControllers.js")

const auth = require('../auth.js');

const router = express.Router();

//this route is responsible for adding products

router.post('/addProduct', auth.verify, productControllers.addProduct);

//route responsible for retrieving all products.
router.get('/', auth.verify, productControllers.getAllProducts);

//route responsible for retriving all active products
router.get('/activeProducts', productControllers.getActiveProducts);

//resposible for retrieving specific product
router.get('/:id', productControllers.getProduct);

//updating product
router.patch('/:productId', auth.verify, productControllers.updateProduct);

//Archive Product (Admin only)
router.patch("/:productId/archive", auth.verify, productControllers.archiveProduct);

module.exports = router;