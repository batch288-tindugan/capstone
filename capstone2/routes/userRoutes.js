const express = require("express");
const userControllers = require('../controllers/userControllers.js');

const auth = require('../auth.js');

const router = express.Router();

//Routes
router.post("/register", userControllers.userRegistration);

//login
router.post("/login", userControllers.loginUser);

//users details
router.get("/details", auth.verify, userControllers.getProfile);

router.get("/userDetails", auth.verify, userControllers.getUserDetails);

module.exports = router;