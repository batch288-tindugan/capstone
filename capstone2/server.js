const express = require("express");

const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./routes/userRoutes.js');

const productRoutes = require('./routes/productRoutes.js');

const orderRoutes = require('./routes/orderRoutes.js');

const app = express();

const port = 4001;

//Mongod DB connection
mongoose.connect("mongodb+srv://admin:admin@batch288tindugan.jnr8rvm.mongodb.net/SweetKekkiAPI?retryWrites=true&w=majority", {useNewUrlParser: true,
	useUnifiedTopology: true
})


let SweetKekkiAPI = mongoose.connection

SweetKekkiAPI.on("error", console.error.bind(
    console,"Error! can't connect to the database"));


SweetKekkiAPI.once("open", ()=> console.log("We're connected to the cloud database!"))

//middlewares
app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.use(cors());

//add the routing of the routes from the userRoutes

app.use('/user', userRoutes);

app.use('/products', productRoutes);

app.use('/orders', orderRoutes);


app.listen(port, () => {
	console.log(`server is running at port ${port}!`);
 })